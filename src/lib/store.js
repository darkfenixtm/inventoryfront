import { writable } from 'svelte/store';

export const fronthash = writable(null);
export const nameUsu = writable(null);
export const person = writable(null);
export const mensaje = writable(null);