import Register from '../routes/register.svelte';

const PUBLIC_BASE_URL = import.meta.env.VITE_PUBLIC_BASE_URL
const PUBLIC_BASE = import.meta.env.VITE_PUBLIC_BASE
import { fronthash, mensaje, nameUsu } from "./store.js";
import { get } from 'svelte/store';

export async function send(tipo, endpoint, body = null) {
    console.log('relyyy ', PUBLIC_BASE_URL)
    let hashfront = get(fronthash)
    let myHeaders = null
    if (hashfront) {
        myHeaders = {
            "Content-Type": 'application/json',
            "Authorization": 'Bearer ' + hashfront
        }
    } else {
        myHeaders = {
            "Content-Type": 'application/json'
        }
    }

    let response = null
    try {
        let ruta = ""
        if (endpoint == '/login') {
            ruta = PUBLIC_BASE_URL + endpoint
        } else {
            ruta = PUBLIC_BASE_URL + PUBLIC_BASE + endpoint
        }
        if (body) {
            response = await fetch(
                ruta, {
                method: tipo,
                headers: myHeaders,
                body: JSON.stringify(body),
            })
        } else {
            response = await fetch(
                ruta, {
                method: tipo,
                headers: myHeaders,
            })
        }

    } catch (error) {
        // mensaje = error 
        console.error('ERROR ', error)

    }
    console.log('re.OK', response.ok)
    const json = await response.json();
    console.log('REsss', json)
    if (!response.ok) {
        mensaje.set(json.error)
        window.alert(json.error)
    }
    // mensaje
    return json
}